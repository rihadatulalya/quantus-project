Source code ini merupakan jawaban dari Tes Kandidat Data Engineer di PT. Quantus Telematika Indonesia.
Adapun rincian dari source code ini adalah sebagai berikut.

## Diagram Skema
![aruyadatabase_-_exercise_quantus](/uploads/cd2148b7d373ecad44370a43fa6c3702/aruyadatabase_-_exercise_quantus.png)

Pada skema di atas, dapat terlihat hubungan antar tabel untuk kebutuhan database form laporan kehilangan barang pada perusahaan.
Penjelasan dari masing-masing tabel:

1. Tabel Laporan Header (feat_laporan_h).
Tabel ini berisikan mengenai informasi kehilangan yang terjadi pada perusahaan. Field yang tercantum dalam tabel ini berupa:
- id_report merupakan id dari setiap laporan yang masuk.
- date_of_report merupakan tanggal kejadian kehilangan dilaporkan pada petugas.
- location_of_area merupakan id tempat atau lokasi pelaporan.
- employeeid merupakan id pegawai yang melapor.
- asset_id merupakan id asset/barang yang hilang

2. Tabel Laporan detail (feat_laporan_d)
Tabel ini berisikan mengenai informasi secara kronologis dari hilangnya barang. Field yang tercantum dalam tabel ini berupa:
- id_report merupakan id dari form kehilangan yang telah dilaporkan (merefer pada tabel feat_laporan_h)
- seqno merupakan urutan atau dari kronologis hilangnya barang.
- time_of_incident merupakan waktu pada saat kejadian berlangsung.
- place_of_incident merupakan tempat kronologis.
- description_of_incident merupakan rincian kronologis dari awal sampai barang hilang.

3. Tabel master_place
Tabel master ini merupakan tabel yang menyimpan data-data tempat di perusahaann. Field yang tercantum dalam tabel ini adalah:
- placeid merupakan id dari tempat/place.
- place_name merupakan nama tempat tersebut.
- create_user merupakan user/penginput data master pada system
- create_date merupakan tanggal penginputan data master pada system
- approved_user merupakan user yang mengapprove data master pada system
- approved_date merupakan tanggal approve data master pada system 
- isactive merupakan flag data tersebut masih active atau tidak pada system. Active disini maksudnya adalah data tersebut masih bisa digunakan atau tidak dalam proses penginputan data transaksi.
- isapprove merupakan flag data tersebut sudah diapprove atau belum.

4. Tabel Master Asset (master_asset)
Tabel ini merupakan tabel yang menyimpan data informasi mengenai asset-asset yang ada di perusahaan tersebut. Field pada tabel ini berupa:
- asset_id merupakan ID dari asset tersebut.
- asset_name merupakan nama dari asset yang dimiliki perusahaan.
- asset_typeid merupakan tipe asset
- asset_classificationid merupakan kelas asset tersebut (ini biasanya berkaitan dengan pengklasifikasian untuk proses pembayaran pajak).
- asset_brandid merupakan brand/merk dari asset
- create_user merupakan user/penginput data master pada system
- create_date merupakan tanggal penginputan data master pada system
- approved_user merupakan user yang mengapprove data master pada system
- approved_date merupakan tanggal approve data master pada system 
- isactive merupakan flag data tersebut masih active atau tidak pada system. Active disini maksudnya adalah data tersebut masih bisa digunakan atau tidak dalam proses penginputan data transaksi.
- isapprove merupakan flag data tersebut sudah diapprove atau belum.

5. Tabel Asset Brand (master_asset_brand)
Tabel ini merupakan tabel master yang menyimpan data informasi mengenai brand. Terdiri dari field-field berikut.
- asset_brandid merupakan ID Brand barang.
- asset_brand_name merupakan nama asset/barang.
- create_user merupakan user/penginput data master pada system
- create_date merupakan tanggal penginputan data master pada system
- approved_user merupakan user yang mengapprove data master pada system
- approved_date merupakan tanggal approve data master pada system 
- isactive merupakan flag data tersebut masih active atau tidak pada system. Active disini maksudnya adalah data tersebut masih bisa digunakan atau tidak dalam proses penginputan data transaksi.
- isapprove merupakan flag data tersebut sudah diapprove atau belum.

6. Tabel Asset Classification (master_asset_classification)
Tabel ini menyimpan data mengenai macam-macam pengelompokan kelas asset. Field yang ada di tabel ini adalah sebagai berikut.
- asset_classificationid merupakan ID dari asset_classification
- asset_classification_name merupakan nama asset clas 
- create_user merupakan user/penginput data master pada system
- create_date merupakan tanggal penginputan data master pada system
- approved_user merupakan user yang mengapprove data master pada system
- approved_date merupakan tanggal approve data master pada system 
- isactive merupakan flag data tersebut masih active atau tidak pada system. Active disini maksudnya adalah data tersebut masih bisa digunakan atau tidak dalam proses penginputan data transaksi.
- isapprove merupakan flag data tersebut sudah diapprove atau belum.

7. Master Asset Type (master_asset_type)
Tabel ini menyimpan data mengenai tipe-tipe barang/asset. Field yang tercantum adalah sebagai berikut.
- asset_typeid merupakan ID tipe barang
- asset_type_name merupakan nama tipe barang
- create_user merupakan user/penginput data master pada system
- create_date merupakan tanggal penginputan data master pada system
- approved_user merupakan user yang mengapprove data master pada system
- approved_date merupakan tanggal approve data master pada system 
- isactive merupakan flag data tersebut masih active atau tidak pada system. Active disini maksudnya adalah data tersebut masih bisa digunakan atau tidak dalam proses penginputan data transaksi.
- isapprove merupakan flag data tersebut sudah diapprove atau belum.


8. Tabel Master Employee (master_employee)
Tabel ini merupakan tabel yang menyimpan data pegawai perusahaan. Field-field yang ada disini adalah:
- employeeid merupakan ID pegawai.
- employee_name merupakan nama pegawai.
- positionid merupakan ID posisi/jabatan pegawai.
- departmentid merupakan ID Departemen pegawai.
- divisionid merupakan ID Divisi pegawai.
- companyid merupakan ID Company pegawai tersebut bekerja.
- create_user merupakan user/penginput data master pada system
- create_date merupakan tanggal penginputan data master pada system
- approved_user merupakan user yang mengapprove data master pada system
- approved_date merupakan tanggal approve data master pada system 
- isactive merupakan flag data tersebut masih active atau tidak pada system. Active disini maksudnya adalah data tersebut masih bisa digunakan atau tidak dalam proses penginputan data transaksi.
- isapprove merupakan flag data tersebut sudah diapprove atau belum.


9. Master Position (master_position)
Tabel ini merupakan tabel yang menyimpan data master posisi/jabatan pegawai. Adapun field-fieldnya adalah sebagai berikut.
- positionid merupakan ID posisi/jabatan pegawai
- position_name merupakan nama posisi/jabatan pegawai.
- level merupakan tingkatan pegawai tersebut. Jika lebih kecil maka dia lebih tinggi jabatannya.
- create_user merupakan user/penginput data master pada system
- create_date merupakan tanggal penginputan data master pada system
- approved_user merupakan user yang mengapprove data master pada system
- approved_date merupakan tanggal approve data master pada system 
- isactive merupakan flag data tersebut masih active atau tidak pada system. Active disini maksudnya adalah data tersebut masih bisa digunakan atau tidak dalam proses penginputan data transaksi.
- isapprove merupakan flag data tersebut sudah diapprove atau belum.

10. Tabel Master Divisi (master_division)
Tabel ini merupakan tabel master yang menyimpan data divisi pegawai. Adapun field-fieldnya adalah sebagai berikut.
- divisionid adalah ID divisi pegawai.
- division_name adalah nama divisi tersebut.
- departmentid adalah ID Department dari divisi tersebut.
- create_user merupakan user/penginput data master pada system
- create_date merupakan tanggal penginputan data master pada system
- approved_user merupakan user yang mengapprove data master pada system
- approved_date merupakan tanggal approve data master pada system 
- isactive merupakan flag data tersebut masih active atau tidak pada system. Active disini maksudnya adalah data tersebut masih bisa digunakan atau tidak dalam proses penginputan data transaksi.
- isapprove merupakan flag data tersebut sudah diapprove atau belum.


11. Tabel Master Company (master_company)
Tabel ini merupakan tabel master yang menyimpan data perusahaan. Adapun field-fieldnya adalah sebagai berikut.
- companyid adalah ID Perusahaan.
- company_name adalah nama perusahaannya.
- city_code adalah kode kota perusahaan tersebut.
- create_user merupakan user/penginput data master pada system
- create_date merupakan tanggal penginputan data master pada system
- approved_user merupakan user yang mengapprove data master pada system
- approved_date merupakan tanggal approve data master pada system 
- isactive merupakan flag data tersebut masih active atau tidak pada system. Active disini maksudnya adalah data tersebut masih bisa digunakan atau tidak dalam proses penginputan data transaksi.
- isapprove merupakan flag data tersebut sudah diapprove atau belum.

12. Tabel Master Department (master_department)
Tabel ini merupakan tabel yang menyimpan data department yang ada. Adapun field-fieldnya adalah sebagai berikut.
- departmentid merupakan ID department.
- department_name merupakan nama departemen.
- create_user merupakan user/penginput data master pada system
- create_date merupakan tanggal penginputan data master pada system
- approved_user merupakan user yang mengapprove data master pada system
- approved_date merupakan tanggal approve data master pada system 
- isactive merupakan flag data tersebut masih active atau tidak pada system. Active disini maksudnya adalah data tersebut masih bisa digunakan atau tidak dalam proses penginputan data transaksi.
- isapprove merupakan flag data tersebut sudah diapprove atau belum.

13. Tabel Master City (master_city)
Tabel ini merupakan tabel master kota. Adapun field-fieldnya adalah sebagai berikut.
- city_code merupakan ID kota tersebut.
- city_name merupakan nama kota.
- province_id merupakan ID Provinsi dimana kota tersebut berada.
- create_user merupakan user/penginput data master pada system
- create_date merupakan tanggal penginputan data master pada system
- approved_user merupakan user yang mengapprove data master pada system
- approved_date merupakan tanggal approve data master pada system 
- isactive merupakan flag data tersebut masih active atau tidak pada system. Active disini maksudnya adalah data tersebut masih bisa digunakan atau tidak dalam proses penginputan data transaksi.
- isapprove merupakan flag data tersebut sudah diapprove atau belum.

14. Tabel Master Province (master_province)
Tabel ini merupakan tabel yang menyimpan data master provinsi. Adapun field-field nya adalah sebagai berikut.
- province_code adalah ID Provinsi.
- province_name adalah nama provinsi tersebut.
- countryid adalah ID negara dimana provinsi tersebut ada.
- create_user merupakan user/penginput data master pada system
- create_date merupakan tanggal penginputan data master pada system
- approved_user merupakan user yang mengapprove data master pada system
- approved_date merupakan tanggal approve data master pada system 
- isactive merupakan flag data tersebut masih active atau tidak pada system. Active disini maksudnya adalah data tersebut masih bisa digunakan atau tidak dalam proses penginputan data transaksi.
- isapprove merupakan flag data tersebut sudah diapprove atau belum.

15. Tabel Master Country ( master_country)
Tabel ini merupakan tabel yang menyimpan data master negara. Adapun field-field yang tercantum dalam tabel ini adalah sebagai berikut.
- countryid merupakan ID negara.
- country_name merupakan nama negara tersebut.
- create_user merupakan user/penginput data master pada system
- create_date merupakan tanggal penginputan data master pada system
- approved_user merupakan user yang mengapprove data master pada system
- approved_date merupakan tanggal approve data master pada system 
- isactive merupakan flag data tersebut masih active atau tidak pada system. Active disini maksudnya adalah data tersebut masih bisa digunakan atau tidak dalam proses penginputan data transaksi.
- isapprove merupakan flag data tersebut sudah diapprove atau belum.

